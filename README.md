Scheduler example plugin for Jira
========================

This Jira plugin is an example on how to create scheduled task in a jira plugin using Spring Scanner components. (components with annotations)

The plugin is loosely based on Chris Fuller examples (https://bitbucket.org/cfuller/atlassian-scheduler-jira-example) 
which is made with the old fashioned components declared in the atlassian-plugin.xml 

This plugin only implements the SchedulerService variant to make it simpler to understand and also drops the AO for the same reason. 

----

There are three main components(classes) to schedule a service: 

MyJobRunnerImpl.java 

- this is the worker service that will do the actual work

MySchedulerServiceImpl.java

- this is a service that offers methods to add and remove a scheduled task and register and unregister the JobRunner
- this is may not be necessary for every case but if you want to start and stop individual scheduled tasks from UI it's nice to have.  
   You can opt to put this code directly in InitPlugin class if your schedule is not changing dynamically 
- the tasks are scheduled to run only on one node if installed on jira data center, but with a change of a constant can be changed to run on each node.
	See comment in code
    
InitPlugin.java

- this will create the initial scheduling and unscheduling when the plugin is installed, uninstalled, enabled, disabled, or when jira started or shut down.

There are few other classes in this example but only for demonstration purposes.

See also comments in code for more information.

---
The plugin was tested on Jira 7.5.2 but it probably work with newer versions(7.x) of jira as well.

	
.	


