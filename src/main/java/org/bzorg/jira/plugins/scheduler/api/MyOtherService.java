package org.bzorg.jira.plugins.scheduler.api;

import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;

@JiraComponent
public interface MyOtherService {

    public void doSomething();
    
}
