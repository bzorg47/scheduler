package org.bzorg.jira.plugins.scheduler.impl;

import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static org.bzorg.jira.plugins.scheduler.api.MyJobRunner.SCHEDULED_JOB_KEY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.inject.Inject;

import org.bzorg.jira.plugins.scheduler.api.MyJobRunner;
import org.bzorg.jira.plugins.scheduler.api.MySchedulerService;
import org.bzorg.jira.plugins.scheduler.model.TaskParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;
import com.google.common.collect.ImmutableMap;

@JiraComponent
public class MySchedulerServiceImpl implements MySchedulerService {

    Logger log = LoggerFactory.getLogger(MySchedulerServiceImpl.class);

    private static final int MAX_JITTER = 10000;
    private static final Random RANDOM = new Random();

    // start scheduler 15 seconds after it is configured
    private static final int MIN_DELAY = 15000;

    @ComponentImport
    private final SchedulerService schedulerService;
    
    private final MyJobRunner myJobRunner;

    @Inject
    public MySchedulerServiceImpl(final SchedulerService schedulerService, MyJobRunner myJobRunner) {
	this.schedulerService = schedulerService;
	this.myJobRunner = myJobRunner;
    }

    // this method will be called once on startup
    @Override
    public void registerJobRunner() {
	schedulerService.registerJobRunner(MyJobRunner.SCHEDULED_JOB_KEY, myJobRunner);
    }

    // this method should be called on plugin disabled
    @Override
    public void unregisterJobRunner() {
	// if your job Ids are likely to change from one plugin start/stop to the next
	// it's better that you remove previously scheduled jobs
	List<JobDetails> jobs = schedulerService.getJobsByJobRunnerKey(SCHEDULED_JOB_KEY);
	for (JobDetails jobDetails : jobs) {
	    schedulerService.unscheduleJob(jobDetails.getJobId());
	}
	// finally unregister your jobhandler
	schedulerService.unregisterJobRunner(MyJobRunner.SCHEDULED_JOB_KEY);
    }

    @Override
    public void createSchedule(TaskParameters scheduleDetails, Long interval) throws Exception {
	// we don't want that all schedules on each node to start at once
	// so we create a random delay between them
	final int jitter = RANDOM.nextInt(MAX_JITTER);

	// we calculate the time of first run.
	final Date firstRun = new Date(System.currentTimeMillis() + MIN_DELAY + jitter);

	// create a map of parameters you want to add to the JobConfig
	// this map should ideally should contain only java primitives, maps,
	// lists and Strings to make sure that its serializable 
	// without the classloader to have access to the plugin's classes
	final Map<String, Serializable> parameters = ImmutableMap.of("scheduleId", (Serializable) scheduleDetails.getId(), "name", (Serializable) scheduleDetails.getName());

	// here you can decide which run mode to use One per cluster(RUN_ONCE_PER_CLUSTER) 
	// or one per node (RUN_LOCALLY)
	final RunMode runMode = RUN_ONCE_PER_CLUSTER;

	// construct a jobConfig ..
	final JobConfig jobConfig = JobConfig.forJobRunnerKey(MyJobRunner.SCHEDULED_JOB_KEY)
					.withSchedule(Schedule.forInterval(interval, firstRun))
					.withRunMode(runMode)
					.withParameters(parameters);

	try {
	    // create a jobId .. this has to be unique
	    final JobId jobId = toJobId(scheduleDetails.getId());
	    log.info("JobId is: {}", jobId);
	    final JobDetails existing = schedulerService.getJobDetails(jobId);
	    if (existing == null) {
		log.info("Scheduler for job " + scheduleDetails.getName() + " does not exist, create it normally");
	    } else {
		log.info("Scheduler for connection " + scheduleDetails.getName()
			+ " already exists, the schedule will be replaced " + existing);
	    }
	    // actually schedule the job
	    schedulerService.scheduleJob(jobId, jobConfig);
	    log.info("Successfully scheduled jobId=" + jobId);
	} catch (SchedulerServiceException sse) {
	    throw new Exception("Unable to create schedule for '" + scheduleDetails.getName() + '\'', sse);
	}
    }

    private JobId toJobId(String detailsId) {
	return JobId.of(InitPlugin.PLUGIN_KEY + "_" + detailsId);
    }

    @Override
    public void removeSchedule(TaskParameters scheduleDetails) throws Exception {

	final JobId jobId = toJobId(scheduleDetails.getId());
	final JobDetails jobDetails = schedulerService.getJobDetails(jobId);
	if (jobDetails != null) {
	    // this would probably not happen
	    if (!SCHEDULED_JOB_KEY.equals(jobDetails.getJobRunnerKey())) {
		throw new Exception("Job key '" + jobDetails.getJobId() + "' does not belong to me!");
	    }
	    schedulerService.unscheduleJob(jobDetails.getJobId());
	}
    }
}