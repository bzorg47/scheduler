package org.bzorg.jira.plugins.scheduler.impl;

import java.io.Serializable;
import java.util.Map;

import javax.inject.Inject;

import org.bzorg.jira.plugins.scheduler.api.MyJobRunner;
import org.bzorg.jira.plugins.scheduler.api.MyOtherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;

@JiraComponent
public class MyJobRunnerImpl implements MyJobRunner {

    Logger log = LoggerFactory.getLogger(MyJobRunnerImpl.class);

    // and arbitrary service in your plugin
    private final MyOtherService myOtherService;

    // add all you services here you need to process the scheduled taks
    @Inject
    public MyJobRunnerImpl(MyOtherService myOtherService) {
	this.myOtherService = myOtherService;
    }

    @Override
    public JobRunnerResponse runJob(JobRunnerRequest request) {
	log.info("Executing scheduled service");
	log.info("JobId is: {}", request.getJobId());
	// you can get the details of the job from the jobconfig
	final Map<String, Serializable> parameters = (Map<String, Serializable>) request.getJobConfig().getParameters();
	log.info("Parameters are {}", parameters);
	try {
	    doYourStuffHere(parameters);
	    log.info("Successfully terminated");
	    return JobRunnerResponse.success("Successfully terminated!");
	} catch (Exception e) {
	    return JobRunnerResponse.failed("Failed!");
	}
    }

    private void doYourStuffHere(Map<String, Serializable> parameters) throws Exception {
	// add code here to execute
	myOtherService.doSomething();
    }
}