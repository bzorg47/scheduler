package org.bzorg.jira.plugins.scheduler.impl;

import javax.inject.Inject;

import org.bzorg.jira.plugins.scheduler.api.MySchedulerService;
import org.bzorg.jira.plugins.scheduler.model.TaskParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;

@JiraComponent
@ExportAsService(LifecycleAware.class)
public class InitPlugin implements LifecycleAware {

    Logger log = LoggerFactory.getLogger(InitPlugin.class);
    
    private static long MINUTE = 60000l; 
    
    public static String PLUGIN_KEY = "org.bzorg.jira.plugins.scheduler"; 

    private final MySchedulerService mySchedulerService;

    @Inject
    public InitPlugin(MySchedulerService mySchedulerService) {
	this.mySchedulerService = mySchedulerService;
    }

    @Override
    public void onStart() {
	log.info("starting onStart");
	mySchedulerService.registerJobRunner();
	TaskParameters parameters = new TaskParameters();
	parameters.setId("JobID");
	parameters.setName("Example Parameters");
	try {
	    mySchedulerService.createSchedule(parameters, MINUTE);
	    log.info("Schedule created");
	} catch (Exception e) {
	    log.info("There was a problem in creating the schedule");
	}
	log.info("finished onStart");
    }

    @Override
    public void onStop() {
	mySchedulerService.unregisterJobRunner();
	log.info("Successfully removed scheduled jobs and unregistered jobrunner");
    }

    /**
     * This is received from the plugin system after the plugin is fully
     * initialized.
     */
    @EventListener
    public void onPluginEnabled(PluginEnabledEvent event) {
	log.info("SCHEDULER PLUGIN ENABLED");
    }

}