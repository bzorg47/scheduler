package org.bzorg.jira.plugins.scheduler.impl;

import org.bzorg.jira.plugins.scheduler.api.MyOtherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;

@JiraComponent
public class MyOtherServiceImpl implements MyOtherService {

    private static Logger log = LoggerFactory.getLogger(MyOtherService.class);
    
    @Override
    public void doSomething() {
	// TODO Auto-generated method stub
	log.info("I'm doing something");
    }

}
