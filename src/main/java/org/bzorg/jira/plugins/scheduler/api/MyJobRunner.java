package org.bzorg.jira.plugins.scheduler.api;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.config.JobRunnerKey;

public interface MyJobRunner extends JobRunner {
  JobRunnerKey SCHEDULED_JOB_KEY = JobRunnerKey.of(MyJobRunner.class.getName());
}