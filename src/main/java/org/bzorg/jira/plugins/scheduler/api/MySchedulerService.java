package org.bzorg.jira.plugins.scheduler.api;

import org.bzorg.jira.plugins.scheduler.model.TaskParameters;

public interface MySchedulerService {
	
	/** will register the JobRunner **/
	public void registerJobRunner();
	
	/** will unregister the JobRunner **/
	public void unregisterJobRunner();

	/** will create the schedule for the job to run 
	 * @throws Exception **/
	public void createSchedule(TaskParameters scheduleDetails, Long interval) throws Exception;
	
	/** will remove the schedule 
	 * @throws Exception **/
	public void removeSchedule(TaskParameters scheduleDetails) throws Exception;
	
}